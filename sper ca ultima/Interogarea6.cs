﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea6 : Form
    {
        public Interogarea6()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sql6 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql6.Open();


            SqlCommand cmd6 = new SqlCommand("SELECT r.reteta_id, r.nume, r.descriere, r.categ_id, r.vegetariana, r.timp_preparare, r.portii FROM RETETA r, SET_INGREDIENTE s, INGREDIENT i WHERE r.reteta_id = s.reteta_id AND s.ingred_id = i.ingred_id AND i.ingredient = '" + comboBox1.Text + "'  AND(s.cantitate < (SELECT s.cantitate FROM SET_INGREDIENTE s, INGREDIENT i WHERE s.reteta_id = (SELECT reteta_id FROM RETETA WHERE nume = '" + comboBox2.Text + "') AND i.ingred_id = s.ingred_id AND i.ingredient = '" + comboBox1.Text + "')); ", sql6);

            using (SqlDataReader reader6 = cmd6.ExecuteReader())
            {

                if (reader6.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader6.Read())
                    {
                        listView1.Items.Add(reader6["reteta_id"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["nume"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["descriere"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["categ_id"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["vegetariana"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["timp_preparare"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader6["portii"].ToString());

                    }
                }

                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }
            sql6.Close();
        }
    

        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.ResetText();
            comboBox2.ResetText();
            listView1.Items.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
