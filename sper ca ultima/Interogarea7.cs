﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea7 : Form
    {
        public Interogarea7()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sql7 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql7.Open();

            SqlCommand cmd7 = new SqlCommand("SELECT categ_id, MIN(timp_preparare) AS 'minim', MAX(timp_preparare) AS 'maxim' FROM reteta WHERE categ_id = " + comboBox1.Text + " GROUP BY categ_id ORDER BY categ_id", sql7);
            using (SqlDataReader reader7 = cmd7.ExecuteReader())
            {

                if (reader7.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;

                    while (reader7.Read())
                    {
                        listView1.Items.Add(reader7["categ_id"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader7["minim"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader7["maxim"].ToString());

                    }
                }
                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }

            sql7.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            comboBox1.ResetText();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
