﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea8 : Form
    {
        public Interogarea8()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sql8 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql8.Open();

            SqlCommand cmd8 = new SqlCommand("SELECT AVG(cantitate) AS 'Cantitatea medie' FROM set_ingrediente i JOIN reteta r ON(i.reteta_id = r.reteta_id) JOIN categorie c on(r.categ_id = c.categ_id) WHERE i.ingred_id = (SELECT ingred_id FROM ingredient WHERE ingredient = '" + comboBox2.Text + "') AND (r.categ_id = " + comboBox1.Text + ")", sql8);
            using (SqlDataReader reader8 = cmd8.ExecuteReader())
            {

                if (reader8.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;

                    while (reader8.Read())
                    {
                        listView1.Items.Add(reader8["Cantitatea medie"].ToString());
                       
                    }
                }
                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }

            sql8.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            comboBox1.ResetText();
            comboBox2.ResetText();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
