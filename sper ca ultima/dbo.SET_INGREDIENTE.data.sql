﻿INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10000, 10, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10000, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10000, 12, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10000, 13, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10000, 14, 2, 'lingurita', 'Se mai poate adauga sare dupa gust');

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 12, 600, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 13, 400, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 15, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 16, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10001, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 17, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 18, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 12, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10100, 19, 5, 'lingura', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 19, 5, 'lingura', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 20, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 21, 6, 'buc', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 29, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 22, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10102, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 23, 800, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 12, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 17, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 15, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10200, 14, 3, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 24, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 11, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 19, 500, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 25, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10201, 22, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10202, 27, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10202, 12, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10202, 15, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10202, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 28, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 29, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 30, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 21, 4, 'buc', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 31, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10300, 32, 1, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10301, 33, 1, 'buc', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10301, 29, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10302, 34, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10302, 29, 1000, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10302, 31, 400, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 35, 1000, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 36, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 37, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 38, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 39, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10400, 14, 1, 'lingurita', 'Se mai adauga sare dupa gust');

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10401, 40, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10401, 41, 500, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10401, 42, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10401, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 46, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 12, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 42, 300, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 43, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 15, 150, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10402, 14, 1, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10500, 44, 700, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10500, 45, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10500, 19, 4, 'lingura', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10500, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10500, 51, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 26, 400, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 41, 200, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 12, 100, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 11, 200, 'ml', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 14, 2, 'lingurita', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 51, 50, 'gr', NULL);

INSERT INTO set_ingrediente(reteta_id, ingred_id, cantitate, um, comentarii)
VALUES(10501, 45, 100, 'gr', NULL);
