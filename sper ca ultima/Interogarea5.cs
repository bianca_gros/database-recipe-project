﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea5 : Form
    {
        public Interogarea5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sql5 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql5.Open();

            SqlCommand cmd5 = new SqlCommand(null, sql5);

            if (textBox1.Text == "D")
            {
                cmd5 = new SqlCommand("SELECT nume, descriere, timp_preparare FROM reteta WHERE vegetariana = 'D' AND timp_preparare >= ALL(SELECT timp_preparare FROM reteta  WHERE vegetariana = 'D')", sql5);

            }
            else if (textBox1.Text == "N")
            {
                cmd5 = new SqlCommand("SELECT nume, descriere, timp_preparare FROM reteta WHERE vegetariana = 'N' AND timp_preparare >= ALL(SELECT timp_preparare FROM reteta  WHERE vegetariana = 'N')", sql5);

            }



            using (SqlDataReader reader5 = cmd5.ExecuteReader())
            {

                if (reader5.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader5.Read())
                    {
                        listView1.Items.Add(reader5["nume"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader5["descriere"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader5["timp_preparare"].ToString());
                       
                    }
                }
                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }
            sql5.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            listView1.Items.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
