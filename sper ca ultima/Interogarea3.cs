﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea3 : Form
    {
        public Interogarea3()
        {
            InitializeComponent();

           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection sql3 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql3.Open();

           
            String cantitate = textBox1.Text;

            SqlCommand cmd3 = new SqlCommand("SELECT DISTINCT r.nume, r.descriere FROM reteta r JOIN set_ingrediente s ON(r.reteta_id = s.reteta_id) JOIN ingredient i ON(s.ingred_id = i.ingred_id) WHERE i.ingredient = '" + comboBox2.Text + "' AND s.cantitate = " + cantitate + " AND s.um = '" + comboBox1.Text + "'", sql3);



            using (SqlDataReader reader3 = cmd3.ExecuteReader())
            {

                if (reader3.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader3.Read())
                    {
                        listView1.Items.Add(reader3["nume"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader3["descriere"].ToString());

                    }
                }
                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }

            sql3.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            comboBox1.ResetText();
            comboBox2.ResetText();
            textBox1.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
