﻿INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10000, 'Supa de pui', 'Traditionala supa de pui', 100, 'N', 120, 6); 

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10001, 'Supa de legume', 'Morcovi, telina, patrunjel, pastarnac fierte si condimentate ce formeaza o infuzie dulce-amaruie', 100, 'D', 45, 6);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10100, 'Ciorba de fasole', NULL, 101, 'N', 180, 6);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10102, 'Ciorba de salata', 'Ciorba de salata cu franjuri de ou si slanina', 101, 'N', 60, 6);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10200, 'Orez cu legume', NULL, 102, 'D', 45, 4);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10201, 'Mamaliga', 'Varianta "stratificata": mamaliga, branza, smantana, mamaliga etc.', 102, 'N', 30, 5);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10202, 'Peste la cuptor cu legume', 'Pastrav in folie de aluminiu cu legume la cuptor', 102, 'N', 80, 5);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10300, 'Prajitura cu mere', NULL, 103, 'D', 45, 8);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10301, 'Budinca de capsuni', 'Se foloseste praf pentru budinca si lapte', 103, 'D', 30, 4);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10302, 'Inghetata cu pepene galben', 'Inghetata de casa din piure de pepene galben', 103, 'D', 40, 6);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10400, 'Salata cu rucola si tofu', 'Rucola proaspata impreuna cu branza tofu, rosii, porumb si ulei de masline', 104, 'D', 10, 2);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10401, 'Salata cu sfecla si cartofi', 'Salata cu cartofi si sfecla fiarta, maioneza', 104, 'D', 40, 4);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10402, 'Salata de boeuf', 'Legumele si carnea fiarta amestecate cu maioneza si condimente', 104, 'N', 30, 4);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10500, 'Varza a la Cluj', 'Varza calita la cuptor cu ceapa, usturoi si smantana', 105, 'D', 40, 4);

INSERT INTO reteta(reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii)
VALUES (10501, 'Tocana de conopida', 'Tocanita de conopida fiarta cu cartofi si morcovi', 105, 'D', 30, 4);