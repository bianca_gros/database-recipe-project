﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sper_ca_ultima
{
    public partial class AplicatieRetete : Form
    {
        public AplicatieRetete()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Interogarea1 query1 = new Interogarea1();
            query1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Interogarea2 query2 = new Interogarea2();
            query2.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form1 conectare = new Form1();
            conectare.Show();
        }

        private void AplicatieRetete_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Interogarea3 query3 = new Interogarea3();
            query3.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Interogarea4 query4 = new Interogarea4();
            query4.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Interogarea5 query5 = new Interogarea5();
            query5.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Interogarea6 query6 = new Interogarea6();
            query6.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Interogarea7 query7 = new Interogarea7();
            query7.Show();

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Interogarea8 query8 = new Interogarea8();
            query8.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if(DialogResult.Yes == MessageBox.Show("Sunteti sigur ca doriti sa parasiti aplicatia?", "Iesire", MessageBoxButtons.YesNoCancel))
            {
                MessageBox.Show("Multumesc pentru atentie!");
                this.Close();
            }
        }

    }
}
