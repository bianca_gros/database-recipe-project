﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace sper_ca_ultima
{
    public partial class Interogarea4 : Form
    {
        public Interogarea4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            comboBox1.ResetText();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sql4 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql4.Open();

            SqlCommand cmd4 = new SqlCommand("SELECT r.nume AS 'Reteta 1', rr.nume AS 'Reteta 2' FROM reteta r JOIN reteta rr ON r.categ_id = rr.categ_id WHERE r.timp_preparare = rr.timp_preparare AND r.nume < rr.nume AND r.categ_id = " + comboBox1.Text + " ", sql4);
            
            using (SqlDataReader reader4 = cmd4.ExecuteReader())
            {

                if (reader4.HasRows)
                {
                    listView1.View = View.Details;
                    listView1.GridLines = true;
                    listView1.FullRowSelect = true;



                    while (reader4.Read())
                    {
                        listView1.Items.Add(reader4["Reteta 1"].ToString());
                        listView1.Items[listView1.Items.Count - 1].SubItems.Add(reader4["Reteta 2"].ToString());

                    }
                }
                else
                {
                    listView1.Items.Add("Nu exista astfel de date");
                }

                listView1.Show();
            }

            sql4.Close();
        }
    }
}
