﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace sper_ca_ultima
{
    public partial class Interogarea1 : Form
    {

      
        public Interogarea1()
        {
            InitializeComponent();

        }

        private void Interogarea1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'database1DataSet.RETETA' table. You can move, or remove it, as needed.
            //this.rETETATableAdapter.Fill(this.database1DataSet.RETETA);

        }

        private void button1_Click(object sender, EventArgs e)
        {
           this.Close();
       
        }


        private void button2_Click(object sender, EventArgs e)
        {

            listView2.Items.Clear();
            textBox1.Clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            SqlConnection sql1 = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;attachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            sql1.Open();
            
            SqlCommand cmd1 = new SqlCommand(null, sql1);

            if (textBox1.Text == "D")
            {
                cmd1 = new SqlCommand("SELECT reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii FROM reteta WHERE vegetariana = 'D' ORDER BY timp_preparare", sql1);

            }
            else if (textBox1.Text == "N")
            {
                cmd1 = new SqlCommand("SELECT reteta_id, nume, descriere, categ_id, vegetariana, timp_preparare, portii FROM reteta WHERE vegetariana = 'N' ORDER BY timp_preparare", sql1);

            }

            

            using (SqlDataReader reader1 = cmd1.ExecuteReader())
            {

                if (reader1.HasRows)
                {
                    listView2.View = View.Details;
                    listView2.GridLines = true;
                    listView2.FullRowSelect = true;



                    while (reader1.Read())
                    {
                        listView2.Items.Add(reader1["reteta_id"].ToString());
                        listView2.Items[listView2.Items.Count - 1].SubItems.Add(reader1["nume"].ToString());
                        listView2.Items[listView2.Items.Count - 1].SubItems.Add(reader1["descriere"].ToString());
                        listView2.Items[listView2.Items.Count - 1].SubItems.Add(reader1["categ_id"].ToString());
                        listView2.Items[listView2.Items.Count - 1].SubItems.Add(reader1["timp_preparare"].ToString());
                        listView2.Items[listView2.Items.Count - 1].SubItems.Add(reader1["portii"].ToString());

                    }
                }
                else
                {
                    listView2.Items.Add("Nu exista astfel de date");
                }

                listView2.Show();
            }
            sql1.Close();
        }

    }
}
